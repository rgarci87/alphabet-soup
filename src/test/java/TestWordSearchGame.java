import domain.WordSearchGameImpl;
import domain.WordSearchGameState;
import domain.WordSearchSolution;
import api.WordSearchGame;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class TestWordSearchGame {

    @Test
    public void testWordSearchSolutionEquals() {
        WordSearchSolution sol1 = new WordSearchSolution(4, 0, 4,3, "GOOD");
        WordSearchSolution sol2 = new WordSearchSolution(4, 0, 4,4, "GOOD");
        WordSearchSolution sol3 = new WordSearchSolution(4, 0, 4,3, "GOOD");


        assertNotEquals(sol1, sol2);
        assertEquals(sol1, sol1);
        assertEquals(sol1, sol3);
    }

    @Test
    public void FileLoadBasicTest() throws Exception {
        WordSearchGame wordSearchGame = new WordSearchGameImpl();
        WordSearchGameState state = wordSearchGame.loadBoard(new FileInputStream(new File("./src/test/resources/TestBasic.txt")));
        WordSearchGameState expected = testBasicBoardExpected();
        assertNotNull(state);
        assertTrue(Arrays.deepEquals(expected.getBoard(), state.getBoard()));
        assertEquals(expected.getTargetWords().size(), state.getTargetWords().size());
        assertTrue(expected.getTargetWords().containsAll(state.getTargetWords()));
    }

    @Test
    public void BasicSolutionsTest() throws Exception {
        WordSearchGame wordSearchGame = new WordSearchGameImpl();
        WordSearchGameState state = testBasicBoardExpected();

        assertNotNull(state.getSolutions());
        assertTrue(state.getSolutions().isEmpty());

        WordSearchGameState stateFinal = wordSearchGame.processSolutions(state);

        assertNotNull(stateFinal);
        assertTrue(Arrays.deepEquals(state.getBoard(), stateFinal.getBoard()));
        assertEquals(state.getTargetWords().size(), stateFinal.getTargetWords().size());
        assertTrue(state.getTargetWords().containsAll(stateFinal.getTargetWords()));

        // Test Solutions
        assertNotNull(stateFinal.getSolutions());
        assertFalse(stateFinal.getSolutions().isEmpty());

        WordSearchSolution sol1 = new WordSearchSolution(0, 0, 4,4, "HELLO");
        WordSearchSolution sol2 = new WordSearchSolution(4, 0, 4,3, "GOOD");
        WordSearchSolution sol3 = new WordSearchSolution(1, 3, 1,1, "BYE");

        assertEquals(3, state.getSolutions().size());
        assertTrue(state.getSolutions().contains(sol1));
        assertTrue(state.getSolutions().contains(sol2));
        assertTrue(state.getSolutions().contains(sol3));
    }

    @Test
    public void BasicSolutionsOutputTest() throws Exception {
        WordSearchGame wordSearchGame = new WordSearchGameImpl();
        WordSearchGameState state = testBasicBoardExpected();
        WordSearchSolution sol1 = new WordSearchSolution(0, 0, 4,4, "HELLO");
        WordSearchSolution sol2 = new WordSearchSolution(4, 0, 4,3, "GOOD");
        WordSearchSolution sol3 = new WordSearchSolution(1, 3, 1,1, "BYE");
        List<WordSearchSolution> solutions = Arrays.asList(sol1, sol2, sol3);
        state.setSolutions(solutions);

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        wordSearchGame.printBoardSolutions(state, out);

        String output = new String(out.toByteArray());

        String expected = "HELLO 0:0 4:4" + System.lineSeparator() +
                "GOOD 4:0 4:3" + System.lineSeparator() +
                "BYE 1:3 1:1" + System.lineSeparator();

        assertEquals(expected, output);
    }

    @Test
    public void RectangleBoardSolutionsTest() throws Exception {
        WordSearchGame wordSearchGame = new WordSearchGameImpl();
        WordSearchGameState state = testRectangleBoard();

        assertNotNull(state.getSolutions());
        assertTrue(state.getSolutions().isEmpty());

        WordSearchGameState stateFinal = wordSearchGame.processSolutions(state);

        assertNotNull(stateFinal);
        assertTrue(Arrays.deepEquals(state.getBoard(), stateFinal.getBoard()));
        assertEquals(state.getTargetWords().size(), stateFinal.getTargetWords().size());
        assertTrue(state.getTargetWords().containsAll(stateFinal.getTargetWords()));

        // Test Solutions
        assertNotNull(stateFinal.getSolutions());
        assertFalse(stateFinal.getSolutions().isEmpty());

        WordSearchSolution sol1 = new WordSearchSolution(0, 0, 0,1, "HI");
        WordSearchSolution sol2 = new WordSearchSolution(0, 0, 1,0, "HI");

        assertEquals(22, state.getSolutions().size());

        Assert.assertTrue(stateFinal.getSolutions().contains(sol1));
        Assert.assertTrue(stateFinal.getSolutions().contains(sol2));
    }

    @Test
    public void NothingFoundBoardSolutionsTest() {
        WordSearchGame wordSearchGame = new WordSearchGameImpl();
        WordSearchGameState state = testNothingFoundBoard();

        assertNotNull(state.getSolutions());
        assertTrue(state.getSolutions().isEmpty());

        WordSearchGameState stateFinal = wordSearchGame.processSolutions(state);

        assertNotNull(stateFinal);
        assertTrue(Arrays.deepEquals(state.getBoard(), stateFinal.getBoard()));
        assertEquals(state.getTargetWords().size(), stateFinal.getTargetWords().size());
        assertTrue(state.getTargetWords().containsAll(stateFinal.getTargetWords()));

        // Test Solutions
        assertNotNull(stateFinal.getSolutions());
        assertTrue(stateFinal.getSolutions().isEmpty());
    }

    private WordSearchGameState testBasicBoardExpected() {
        WordSearchGameState state = new WordSearchGameState(5,5);

        char[][] board = {
                {'H','A', 'S', 'D', 'F'},
                {'G','E', 'Y', 'B', 'H'},
                {'J','K', 'L', 'Z', 'X'},
                {'C','V', 'B', 'L', 'N'},
                {'G','O', 'O', 'D', 'O'}
        };

        state.setBoard(board);
        state.getTargetWords().add("HELLO");
        state.getTargetWords().add("GOOD");
        state.getTargetWords().add("BYE");

        return state;
    }


    private WordSearchGameState testRectangleBoard() {
        WordSearchGameState state = new WordSearchGameState(4,3);

        char[][] board = {
                {'H','I', 'I'},
                {'I','H', 'H'},
                {'H','I', 'I'},
                {'I','H', 'H'}
        };

        state.setBoard(board);
        state.getTargetWords().add("HI");
        state.getTargetWords().add("HIHI");

        return state;
    }

    private WordSearchGameState testMinimalBoard() {
        WordSearchGameState state = new WordSearchGameState(1,1);

        char[][] board = {
                {'H'}
        };

        state.setBoard(board);
        state.getTargetWords().add("H");

        return state;
    }

    private WordSearchGameState testNothingFoundBoard() {
        WordSearchGameState state = new WordSearchGameState(5,5);

        char[][] board = {
                {'H','A', 'S', 'D', 'F'},
                {'G','E', 'Y', 'B', 'H'},
                {'J','K', 'L', 'Z', 'X'},
                {'C','V', 'B', 'L', 'N'},
                {'G','O', 'O', 'D', 'O'}
        };

        state.setBoard(board);
        state.getTargetWords().add("NOTFOUND");

        return state;
    }

}
