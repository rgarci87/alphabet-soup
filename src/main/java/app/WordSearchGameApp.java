package app;

import domain.WordSearchGameImpl;
import domain.WordSearchGameState;
import api.WordSearchGame;

import java.io.File;
import java.io.FileInputStream;

public class WordSearchGameApp {

    public static void main(String[] args) {
        try {
            WordSearchGame wordSearchGame = new WordSearchGameImpl();
            // Read to parse array size, create Two-Dimension Array containing puzzle, load target word solutions
            WordSearchGameState game = wordSearchGame.loadBoard(new FileInputStream(new File(args[0])));
            // Find solution locations.
            game = wordSearchGame.processSolutions(game);
            // Present Output
            wordSearchGame.printBoardSolutions(game, System.out);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}