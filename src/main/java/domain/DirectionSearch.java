package domain;

/**
 * Processing operation for searching a two-dimensional array of characters in a
 * particular vector cardinal direction for a target string from a given starting location.
 */
public class DirectionSearch {

    private StringBuilder currentString = new StringBuilder();

    private Direction direction;

    private boolean found = false; // flag to truncate search if the

    private int endRowIndex;

    private int endColIndex;

    public DirectionSearch(Direction direction) {
        this.direction = direction;
    }

    public boolean isFound() {
        return found;
    }

    public void setFound(boolean found) {
        this.found = found;
    }

    public int getEndRowIndex() {
        return endRowIndex;
    }

    public void setEndRowIndex(int endRowIndex) {
        this.endRowIndex = endRowIndex;
    }

    public int getEndColIndex() {
        return endColIndex;
    }

    public void setEndColIndex(int endColIndex) {
        this.endColIndex = endColIndex;
    }

    public void search(final char[][] board, final String target, final int startRowIndex,
                       final int startColIndex, final int rowSize, final int colSize) {

        boolean failed = false;  // flag to stop search if the substring no longer matches the beginning of the target

        currentString.append(board[startRowIndex][startColIndex]);

        if (currentString.toString().equals(target)) {
            endRowIndex = startRowIndex;
            endColIndex = startColIndex;
            found = true;
            return;
        }

        int prevRowIndex = startRowIndex;
        int prevColIndex = startColIndex;
        int nextRowIndex = 0;
        int nextColIndex = 0;

        while(!found && !failed) {
            switch (direction) {
                case NORTH:
                    nextRowIndex = prevRowIndex - 1;
                    nextColIndex = prevColIndex;
                    break;
                case SOUTH:
                    nextRowIndex = prevRowIndex + 1;
                    nextColIndex = prevColIndex;
                    break;
                case EAST:
                    nextRowIndex = prevRowIndex;
                    nextColIndex = prevColIndex + 1;
                    break;
                case WEST:
                    nextRowIndex = prevRowIndex;
                    nextColIndex = prevColIndex - 1;
                    break;
                case NORTHWEST:
                    nextRowIndex = prevRowIndex - 1;
                    nextColIndex = prevColIndex - 1;
                    break;
                case NORTHEAST:
                    nextRowIndex = prevRowIndex - 1;
                    nextColIndex = prevColIndex + 1;
                    break;
                case SOUTHWEST:
                    nextRowIndex = prevRowIndex + 1;
                    nextColIndex = prevColIndex - 1;
                    break;
                case SOUTHEAST:
                    nextRowIndex = prevRowIndex + 1;
                    nextColIndex = prevColIndex + 1;
                    break;
            }

            // Search has gone out of array bounds, stop the current search
            if (!(nextRowIndex >= 0 && nextRowIndex < rowSize && nextColIndex >= 0 && nextColIndex < colSize)) {
                failed = true;
                continue;
            }

            currentString.append(board[nextRowIndex][nextColIndex]);

            // Search will not produce the target string, stop the current search
            if (!target.startsWith(currentString.toString())) {
                failed = true;
                continue;
            }

            // Search found the target string, save the final index locations and end the search
            if (currentString.toString().equals(target)) {
                setFound(true);
                setEndRowIndex(nextRowIndex);
                setEndColIndex(nextColIndex);
            }
            else {
                // Setup the start of the next iteration
                prevRowIndex = nextRowIndex;
                prevColIndex = nextColIndex;
            }
        }
    }
}