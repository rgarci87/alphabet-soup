package domain;

import api.WordSearchGame;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class WordSearchGameImpl implements WordSearchGame {

    @Override
    public WordSearchGameState loadBoard(InputStream inputStream) {
        WordSearchGameState board = null;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            // Get Board Size.  First Line ex. 5x5
            String boardSizeStr = br.readLine();
            String[] lengthAndWidth = boardSizeStr.split("x");
            int length = Integer.parseInt(lengthAndWidth[0]);
            int width = Integer.parseInt(lengthAndWidth[1]);
            board = new WordSearchGameState(length, width);

            // Populate board.  Read lines equal to length field.
            for (int i = 0; i < length; i++) {
                String row = br.readLine();
                char[] rowChars = row.trim().toUpperCase().replace(" ", "").toCharArray();
                board.getBoard()[i] = rowChars;
            }

            // Populate Target Word Solutions.  Any number of these can exist.  Go to end of file.
            while (br.ready()) {
                String word = br.readLine().toUpperCase().trim();
                if (!word.isEmpty())
                    board.getTargetWords().add(word);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return board;
    }

    @Override
    public WordSearchGameState processSolutions(WordSearchGameState wordSearchGameState) {

        Map<Character, List<String>> wordCandidateFirstLetters =
                wordSearchGameState.getTargetWords()
                        .stream()
                        .collect(Collectors.groupingBy((word) -> word.charAt(0)));

        Map<String, List<WordSearch>> candidateWordLocations = new HashMap<>();

        // Identify Starting Locations for searches.  Create one search operation for each starting character found.
        for (int rowIndex = 0; rowIndex < wordSearchGameState.getRowSize(); rowIndex++) {

            for (int colIndex = 0; colIndex < wordSearchGameState.getColSize(); colIndex++) {

                Character currentChar = wordSearchGameState.getBoard()[rowIndex][colIndex];

                if (wordCandidateFirstLetters.containsKey(currentChar)) {
                    for (String word : wordCandidateFirstLetters.get(currentChar)) {
                        candidateWordLocations.computeIfAbsent(word, x -> new ArrayList<>())
                                .add(new WordSearch(rowIndex, colIndex, word));
                    }
                }
            }
        }

        List<WordSearch> wordSearches =
                candidateWordLocations.values().stream().flatMap(List::stream).collect(Collectors.toList());

        // Process Vector Cardinal domain.Direction Searches.
        wordSearches.parallelStream().forEach(wordSearch -> {
            wordSearch.getDirectionSearches().parallelStream().forEach((search) -> {
                search.search(
                        wordSearchGameState.getBoard(),
                        wordSearch.getTarget(),
                        wordSearch.getStartRowIndex(),
                        wordSearch.getStartColIndex(),
                        wordSearchGameState.getRowSize(),
                        wordSearchGameState.getColSize());
            });
        });

        // Generate a list of solutions and add them to the game state.
        List<WordSearchSolution> solutions = new ArrayList<>();

        for (WordSearch wordSearch: wordSearches) {
            for (DirectionSearch directionSearch: wordSearch.getDirectionSearches()) {
                if (directionSearch.isFound()) {
                    WordSearchSolution solution = new WordSearchSolution(wordSearch.getStartRowIndex(),
                            wordSearch.getStartColIndex(), directionSearch.getEndRowIndex(),
                            directionSearch.getEndColIndex(), wordSearch.getTarget());
                    solutions.add(solution);
                }
            }
        }

        wordSearchGameState.setSolutions(solutions);

        return wordSearchGameState;
    }

    @Override
    public void printBoardSolutions(WordSearchGameState wordSearchGameState, OutputStream outputStream) {
        try (PrintWriter writer = new PrintWriter(outputStream)) {
            wordSearchGameState.getSolutions().forEach( (wordSearchSolution -> {
                writer.println(wordSearchSolution.target + " " +
                        wordSearchSolution.getStartRowIndex() + ":" +
                        wordSearchSolution.getStartColIndex() + " " +
                        wordSearchSolution.getEndRowIndex() + ":" +
                        wordSearchSolution.getEndColIndex());
            }));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}