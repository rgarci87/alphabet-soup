package domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WordSearchGameState {

    private char[][] board;

    private Set<String> targetWords;

    private int rowSize;

    private int colSize;

    private List<WordSearchSolution> solutions;

    public WordSearchGameState(int rowSize, int colSize) {
        this.rowSize = rowSize;
        this.colSize = colSize;
        this.board = new char[rowSize][colSize];
        this.targetWords = new HashSet<>();
        this.solutions = new ArrayList<>();
    }

    public char[][] getBoard() {
        return board;
    }

    public void setBoard(char[][] board) {
        this.board = board;
    }

    public Set<String> getTargetWords() {
        return targetWords;
    }

    public void setTargetWords(Set<String> targetWords) {
        this.targetWords = targetWords;
    }

    public int getRowSize() {
        return rowSize;
    }

    public void setRowSize(int rowSize) {
        this.rowSize = rowSize;
    }

    public int getColSize() {
        return colSize;
    }

    public void setColSize(int colSize) {
        this.colSize = colSize;
    }

    public List<WordSearchSolution> getSolutions() {
        return solutions;
    }

    public void setSolutions(List<WordSearchSolution> solutions) {
        this.solutions = solutions;
    }
}