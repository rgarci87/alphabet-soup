package domain;

public class WordSearchSolution {

    int startRowIndex;

    int startColIndex;

    int endRowIndex;

    int endColIndex;

    String target;

    public WordSearchSolution(int startRowIndex, int startColIndex, int endRowIndex, int endColIndex, String target) {
        this.startRowIndex = startRowIndex;
        this.startColIndex = startColIndex;
        this.endRowIndex = endRowIndex;
        this.endColIndex = endColIndex;
        this.target = target;
    }

    public int getStartRowIndex() {
        return startRowIndex;
    }

    public void setStartRowIndex(int startRowIndex) {
        this.startRowIndex = startRowIndex;
    }

    public int getStartColIndex() {
        return startColIndex;
    }

    public void setStartColIndex(int startColIndex) {
        this.startColIndex = startColIndex;
    }

    public int getEndRowIndex() {
        return endRowIndex;
    }

    public void setEndRowIndex(int endRowIndex) {
        this.endRowIndex = endRowIndex;
    }

    public int getEndColIndex() {
        return endColIndex;
    }

    public void setEndColIndex(int endColIndex) {
        this.endColIndex = endColIndex;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WordSearchSolution that = (WordSearchSolution) o;

        if (getStartRowIndex() != that.getStartRowIndex()) return false;
        if (getStartColIndex() != that.getStartColIndex()) return false;
        if (getEndRowIndex() != that.getEndRowIndex()) return false;
        if (getEndColIndex() != that.getEndColIndex()) return false;
        return getTarget().equals(that.getTarget());
    }

    @Override
    public int hashCode() {
        int result = getStartRowIndex();
        result = 31 * result + getStartColIndex();
        result = 31 * result + getEndRowIndex();
        result = 31 * result + getEndColIndex();
        result = 31 * result + getTarget().hashCode();
        return result;
    }
}
