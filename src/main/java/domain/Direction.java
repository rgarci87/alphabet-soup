package domain;

/**
 * Represents cardinal direction of vectors while searching for a word match in a matrix so that searches
 * continue in same direction.
 */
public enum Direction {
    NORTH,
    SOUTH,
    EAST,
    WEST,
    NORTHWEST,
    NORTHEAST,
    SOUTHWEST,
    SOUTHEAST,
}