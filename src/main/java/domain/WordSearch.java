package domain;

import domain.DirectionSearch;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Bean to represent a search of all cardinal vector directions within a two dimensional-array for given start
 * index locations.
 */
public class WordSearch {

    private int startRowIndex;

    private int startColIndex;

    private List<DirectionSearch> directionSearches;

    private String target;

    public WordSearch(int startRowIndex, int startColIndex, String target) {
        this.target = target;
        this.startRowIndex = startRowIndex;
        this.startColIndex = startColIndex;
        directionSearches = Arrays.stream(Direction.values())
                .map((DirectionSearch::new)).collect(Collectors.toList());
    }

    public int getStartRowIndex() {
        return startRowIndex;
    }

    public void setStartRowIndex(int startRowIndex) {
        this.startRowIndex = startRowIndex;
    }

    public int getStartColIndex() {
        return startColIndex;
    }

    public void setStartColIndex(int startColIndex) {
        this.startColIndex = startColIndex;
    }

    public List<DirectionSearch> getDirectionSearches() {
        return directionSearches;
    }

    public void setDirectionSearches(List<DirectionSearch> directionSearches) {
        this.directionSearches = directionSearches;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
