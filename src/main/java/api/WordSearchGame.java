package api;

import domain.WordSearchGameState;

import java.io.InputStream;
import java.io.OutputStream;

public interface WordSearchGame {

    /**
     * Reads an AlphabetSoup
     * @param inputStream stream containing an alphabet soup file formatted contents
     * @return game state populated with dimensions, game board and target word solutions
     */
    WordSearchGameState loadBoard(InputStream inputStream);

    /**
     * Populates the game state with the solutions for the locations of the target words
     * for the puzzle.
     * @param wordSearchGameState puzzle information
     * @return state updated with puzzle solutions
     */
    WordSearchGameState processSolutions(WordSearchGameState wordSearchGameState);

    /**
     * Prints the Alphabet solution to the supplied output stream
     * @param wordSearchGameState container of solutions
     * @param outputStream stream to print solutions
     */
    void printBoardSolutions(WordSearchGameState wordSearchGameState, OutputStream outputStream);

}